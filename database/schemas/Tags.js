module.exports = class tags{
    constructor(id,title,icon){
        this.id = id
        this.title = title
        this.icon = icon

    }
    static tableName = 'tags'
    static fields = `title, icon`
    toInsertDbValues() {
        return `'${this.title}', '${this.icon}'`
    }

    toUpdateDbValues() {
        return `title='${this.title}',icon='${this.icon}'`
    }
}
