const TagsSchema = require('../database/schemas/Tags')
const db = require('../database/config')
const tags = require('../controllers/tags')

const table = TagsSchema.tableName
const fields = TagsSchema.fields

class TagsRepository {
    static async list() {
        const query = `select * from ${TagsSchema.tableName}`
        return await db.queryValues(query)
    }

    static async create(values) {
        const query = `insert into ${TagsSchema.tableName} (${fields}) values (${values})`
        return await db.queryValues(query)
    }

    static async get(id) {
        const condition = `where tags_id = ${id}`
        const query = `select * from ${TagsSchema.tableName} ${condition}`
        return await db.queryValues(query)
    }

    static async update(id, values) {
        console.log(id)
        const condition = `where tags_id = ${id}`
        const query = `update ${TagsSchema.tableName} set ${values} ${condition}`
        console.log(query)
        return await db.queryValues(query)
        
    }

    static async delete(id) {
        const condition = `where tags_id = ${id}`
        const query = `delete from ${TagsSchema.tableName} ${condition}`
        return await db.queryValues(query)
    }
}

module.exports = TagsRepository