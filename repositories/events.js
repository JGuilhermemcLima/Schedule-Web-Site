const EventsSchema = require('../database/schemas/Events')
const db = require('../database/config')

const table = EventsSchema.tableName
const fields = EventsSchema.fields

class EventsRepository {
    static async list() {
        const query = `select * from ${EventsSchema.tableName}`
        return await db.queryValues(query)
    }

    static async create(values) {
        const query = `insert into ${EventsSchema.tableName} (${fields}) values (${values})`
        return await db.queryValues(query)
    }

    static async get(id) {
        const condition = `where event_id = ${id}`
        const query = `select * from ${EventsSchema.tableName} ${condition}`
        return await db.queryValues(query)
    }

    static async update(id, values) {
        const condition = `where event_id = ${id}`
        const query = `update ${EventsSchema.tableName} set ${values} ${condition}`
        return await db.queryValues(query)
    }

    static async delete(id) {
        const condition = `where event_id = ${id}`
        const query = `delete from ${EventsSchema.tableName} ${condition}`
        return await db.queryValues(query)
    }
}

module.exports = EventsRepository